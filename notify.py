# flask api to send notifications and send group messages

import jwt
import base64
import datetime
from io import BytesIO
from flask_cors import CORS
from Crypto.Cipher import AES
from bson.json_util import dumps
from flask import Flask, request, jsonify
from models import User, Group, ServerInfo
from bot import send_msg, my_bot, send_photo, send_document
from groups import create_group, add_member_to_group, get_uid_from_phone


# flask app
app = Flask(__name__)


def custom_jsonify(data):
    """
    custom json response
    """
    res = app.make_response(dumps(data))
    res.headers['Content-Type'] = 'application/json'
    res.headers['Accept'] = 'application/json'
    return res


def encryptPassword(password):
    """
    encrypt string to AES cipher string for storing password
    """
    password = password.rjust(16)
    secret_key = 'MySONBOTKey12345'
    cipher = AES.new(secret_key, AES.MODE_ECB)
    encoded = base64.b64encode(cipher.encrypt(password))
    return encoded.decode("utf-8")


def decryptPassword(password):
    """
    decrypt encrypted password string to plain string
    """
    decoded = cipher.decrypt(base64.b64decode(encoded))
    return decoded.strip()


def encodeAuthToken(user_id):
    """
    create jwt token for authentication
    """
    try:
        payload = {
            'sub': user_id,
        }
        token = jwt.encode(payload, 'super-secret-key', algorithm='HS256')
        return token.decode("utf-8")
    except Exception as e:
        return e


class Error(Exception):
    """
    error class to handle api exception
    """
    status_code = 400

    def __init__(self, message, status_code=None, payload=None):
        Exception.__init__(self)
        self.message = message
        if status_code is not None:
            self.status_code = status_code
        self.payload = payload

    def to_dict(self):
        rv = dict(self.payload or ())
        rv['error'] = self.message
        rv['ok'] = 0
        return rv


# register flask api error class
@app.errorhandler(Error)
def handle_invalid_usage(error):
    response = jsonify(error.to_dict())
    response.status_code = error.status_code
    return response


@app.route('/auth/login/', methods=['POST'])
def loginAndGenerateToken():
    """
    login view for frontend
    """
    req_json = request.get_json()
    username = req_json['username']
    password = req_json['password']

    user = User().get(username=username)
    if not user:
        user = User().get(phone_num=username)

    if not user:
        msg = "Invalid username or password"
        raise Error(msg)

    encoded_password = encryptPassword(password.strip())

    if (user["password"]) != encoded_password:
        msg = "Invalid Password"
        raise Error(msg)

    token = encodeAuthToken(user['phone_num'])

    return jsonify({
        'status': 'success',
        'auth_token': token,
        'uid': user['uid'],
        'username': user['username'],
        'is_admin': user['is_admin']
    })


@app.route('/api/notify/', methods=['POST'])
def send_notification():
    """
    send notification to given telegram user
    """
    request_data = request.get_json()
    username = request_data.get("username", "")
    to_id = request_data.get("send_to", "")
    text = request_data.get("message", "")
    msg_type = request_data.get("type", None)  # "HTML" or "Markdown"

    if not username:
        msg = "required parameter username missing"
        raise Error(msg)

    if not to_id:
        msg = "required parameter to missing"
        raise Error(msg)

    if not text:
        msg = "message can not be blank"
        raise Error(msg)

    user_obj = User()
    user = user_obj.get(username=username)

    if not user:
        msg = "you dont have permisson to send notification"
        raise Error(msg)

    group_obj = Group()
    to = group_obj.get(gid=to_id)
    if not to:
        to = user_obj.get(uid=int(to_id))
        if not to:
            msg = "invalid to id"
            raise Error(msg)
        else:
            send_msg(my_bot, user["uid"], to["uid"], text, msg_type)
            return jsonify({"ok": 1})
    GID = "-{0}".format(to["gid"])
    send_msg(my_bot, user["uid"], GID, text, msg_type)
    return jsonify({"ok": 1})


@app.route('/api/photo/', methods=['POST'])
def send_image():
    """
    send image/photo to given telegram user
    """
    username = request.form["username"]
    to_id = request.form["send_to"]
    photo_file = request.files.get("image", "")
    photo = request.form.get("image", "")

    if not username:
        msg = "username missing"
        raise Error(msg)

    if not to_id:
        msg = "to id missing"
        raise Error(msg)

    if not any([photo_file, photo]):
        msg = "photo can not be blank"
        raise Error(msg)

    user_obj = User()
    user = user_obj.get(username=username)

    if not user:
        msg = "you dont have permisson to send notification"
        raise Error(msg)

    group_obj = Group()
    to = group_obj.get(gid=to_id)
    if not to:
        to = user_obj.get(uid=int(to_id))
        if not to:
            msg = "invalid to id"
            raise Error(msg)
        else:
            if photo:
                send_photo(my_bot, user["uid"], to["uid"], photo)
            else:
                out = BytesIO()
                out.write(photo_file.stream.read())
                out.seek(0)
                send_photo(my_bot, user["uid"], to["uid"], out)
            return jsonify({"ok": 1})
    GID = "-{0}".format(to["gid"])
    if photo:
        send_photo(my_bot, user["uid"], GID, photo)
    else:
        out = BytesIO()
        out.write(photo_file.stream.read())
        out.seek(0)
        send_photo(my_bot, user["uid"], GID, out)
    return jsonify({"ok": 1})


@app.route('/api/document/', methods=['POST'])
def upload_document():
    """
    send document to given telegram user
    """
    username = request.form["username"]
    to_id = request.form["send_to"]
    document_file = request.files.get("document", "")

    if not username:
        msg = "username missing"
        raise Error(msg)

    if not to_id:
        msg = "to id missing"
        raise Error(msg)

    if not document_file:
        msg = "document can not be blank"
        raise Error(msg)

    user_obj = User()
    user = user_obj.get(username=username)

    if not user:
        msg = "you dont have permisson to send notification"
        raise Error(msg)

    group_obj = Group()
    to = group_obj.get(gid=to_id)
    if not to:
        to = user_obj.get(uid=int(to_id))        
        if not to:
            msg = "invalid to id"
            raise Error(msg)
        else:
            out = BytesIO()
            out.write(document_file.stream.read())
            out.seek(0)
            send_document(my_bot, user["uid"], to[
                          "uid"], out, str(document_file.filename))
            return jsonify({"ok": 1})
    out = BytesIO()
    out.write(document_file.stream.read())
    out.seek(0)
    GID = "-{0}".format(to["gid"])
    send_document(my_bot, user["uid"], GID,
                  out, str(document_file.filename))
    return jsonify({"ok": 1})


@app.route('/api/create-group/', methods=['POST'])
def create_group_api():
    """
    create telegram group api
    """
    request_data = request.get_json()
    group_name = request_data.get("group_name")
    group_members = request_data.get("group_members")
    uids = [int(i) for i in group_members]
    result, error = create_group(uids, group_name)
    if not error:
        result = result.to_dict()
        group_id = result['chats'][0]['id']
        group = Group()
        group.create(gid=str(group_id), group_name=group_name, members=uids)
        return jsonify({"msg": "Group created successfully.", "error": False})
    return jsonify({"msg": "Group not created.", "error": True})


@app.route('/api/users/', methods=['get'])
def get_users():
    """
    api view to get list of users
    """
    u = User()
    users = u.filter()
    return custom_jsonify({"users": users})


@app.route('/api/groups/', methods=['get'])
def get_groups():
    """
    get list of groups
    """
    g = Group()
    groups = g.filter()
    groups_data = []
    for group in groups:
        u = User()
        users = u.filter(**{"uid": {"$in": [i for i in group["members"]] } })
        group["members"] = []
        for user in users:
            group["members"].append("{0} {1}".format(user["first_name"], user["last_name"]))
        groups_data.append(group)
    return custom_jsonify({"groups": groups_data})


@app.route('/api/users-list/', methods=['get'])
def users_list():
    """
    users list api for multiselect
    """
    res_data = []
    u = User()
    users = u.filter()
    for user in users:
        data = {}
        data["id"] = user["uid"]
        data["name"] = "{0} {1}".format(user["first_name"], user["last_name"])
        res_data.append(data)

    g = Group()
    groups = g.filter()
    for group in groups:
        data = {}
        data["id"] = group["gid"]
        data["name"] = group["group_name"]
        res_data.append(data)
    return custom_jsonify({"users": res_data})


@app.route('/api/create-user/', methods=['post'])
def create_user():
    """
    create new user api view
    """
    request_data = request.get_json()
    uid = request_data.get("uid")
    phone_num = request_data.get("phone_num")
    first_name = request_data.get("first_name")
    last_name = request_data.get("last_name")
    username = request_data.get("username")
    is_admin = request_data.get("is_admin")
    password = request_data.get("password")
    cnfpassword = request_data.get("cnfpassword")
    encoded_password = encryptPassword(password)
    u = User()
    data = {"uid": int(uid),
            "first_name": first_name,
            "last_name": last_name,
            "phone_num": phone_num,
            "username": username,
            "password": encoded_password,
            "is_admin": is_admin
            }
    user = u.create(**data)
    return custom_jsonify({"user": "user"})


@app.route('/api/edit-user/', methods=['post'])
def edit_user():
    """
    edit user api view
    """
    request_data = request.get_json()
    uid = request_data.get("uid")
    phone_num = request_data.get("phone_num")
    first_name = request_data.get("first_name")
    last_name = request_data.get("last_name")
    username = request_data.get("username")
    is_admin = request_data.get("is_admin")
    is_password_update = request_data.get("is_password_update")
    password = request_data.get("password")
    cnfpassword = request_data.get("cnfpassword")
    u = User()
    user = u.get(**{"uid": int(uid)})
    if user:
        user["uid"] = int(uid)
        user["first_name"] = first_name
        user["last_name"] = last_name
        user["phone_num"] = phone_num
        user["username"] = username
        user["is_admin"] = is_admin
        if is_password_update:
            encoded_password = encryptPassword(password)
            user["password"] = encoded_password
        u.collection.save(user)
        res = {
            "msg": "User updated successfully.",
            "error": False
        }
    else:
        res = {
            "msg": "No such user found.",
            "error": True
        }
    return jsonify({"result": res})


@app.route("/api/get-uid", methods=["post"])
def get_uid():
    """
    get telegram uid for given phone number and name
    """
    request_data = request.get_json()
    phone_num = request_data.get("phone_num")
    first_name = request_data.get("first_name")
    last_name = request_data.get("last_name")
    uid = get_uid_from_phone(phone_num, first_name, last_name)
    if not uid:
        data = {"uid": uid,
                "error": True,
                "msg": "Invalid Phone Number no such user found."
                }
    else:
        data = {"uid": uid,
                "error": False,
                "msg": ""
                }
    return jsonify({"uid": data})


@app.route('/api/user/<uid>/', methods=['get'])
def get_user(uid):
    """
    get user instance for given uid
    """
    u = User()
    try:
        uid = int(uid)
    except:
        pass
    user = u.get(**{"uid": uid})
    return custom_jsonify({"user": user})


@app.route('/api/delete-user/<uid>/', methods=['get'])
def delete_user(uid):
    """
    delete user for given uid
    """
    u = User()
    try:
        uid = int(uid)
    except:
        pass
    u.delete(**{"uid": uid})
    return jsonify({"msg": "User deleted successfully", "error": False})


@app.route('/api/create-server/', methods=['POST'])
def create_server_api():
    """
    add new server information
    """
    request_data = request.get_json()
    server_name = request_data.get("server_name")
    server_ip = request_data.get("server_ip")
    server_username = request_data.get("server_username")
    server_password = request_data.get("server_password")
    server_access = request_data.get("server_access")
    server_access = [int(i) for i in server_access]
    server_obj = ServerInfo()
    server_obj.create(server_ip=server_ip,
                      server_name=server_name,
                      server_username=server_username,
                      server_password=server_password,
                      users=server_access)
    return jsonify({"msg": "Group created successfully.", "error": False})


@app.route('/api/servers/', methods=['get'])
def get_servers():
    """
    get list of servers information
    """
    s = ServerInfo()
    servers = s.filter()
    servers_data = []
    for server in servers:
        u = User()
        users = u.filter(**{"uid": {"$in": [i for i in server["users"]] } })
        server["users"] = []
        for user in users:
            server["users"].append("{0} {1}".format(user["first_name"], user["last_name"]))
        servers_data.append(server)
    return custom_jsonify({"servers": servers_data})


if __name__ == '__main__':
    # run flask app server
    app.run()
