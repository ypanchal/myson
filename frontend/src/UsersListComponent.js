import React, { Component }  from 'react';
import {Grid, Row, Table, Button, Alert} from 'react-bootstrap';
import {loginRequired, adminRequired} from './LoginRequiredMixin.js';


class UsersListComponent extends Component {

	constructor(props) {
		super(props);

	    this.state = {
	    	users: [],
	    	msg: false
	    };
	}

	handleDismiss = (event) => {
		const state = this.state
		state["msg"] = false;
		this.setState(state);
	}

	fetchUsers() {
		var that = this;
		fetch("http://localhost:5000/api/users/").then(function (response) {
			return response.json();
		}).then(function (result) {
			that.setState({users: result.users});
		});
	}

	deleteUser(uid) {
		var that = this;
		var url = "http://localhost:5000/api/delete-user/" + uid;
		fetch(url).then(res => {
			if(res.status === 200) {
				var new_users = []
				that.state.users.map(function(user){
					if(user.uid !== uid){
						new_users.push(user)
					}
				});
				this.setState(that.setState({users: new_users, msg: true}));
			} else {
				this.setState(that.state);
			}
		});
	}

	componentWillMount() {
		loginRequired();
		adminRequired();
		this.fetchUsers("reactjs");
	}

	render() {
		const is_msg = this.state.msg;
	    return (
	    	<div>
	    		<Grid>
		    		<Row>
		          		<h2>Users</h2>
		          		{ is_msg &&
		      				<Alert bsStyle="success" onDismiss={this.handleDismiss}>
		      					<p>
		      						User deleted Successfully.
		      					</p>
					        </Alert>
				    	}
		          		<Table striped bordered condensed hover>
		          			<thead>
			          			<tr>
									<th>#</th>
									<th>UID</th>
									<th>First Name</th>
									<th>Last Name</th>
									<th>Username</th>
									<th>Mobile Number</th>
									<th>Edit</th>
									<th>Delete</th>
								</tr>
							</thead>
							<tbody>
								{this.state.users.map((user, index) =>
									<tr>
										<td>{index+1}</td>
										<td>{user.uid}</td>
										<td>{user.first_name}</td>
										<td>{user.last_name}</td>
										<td>{user.username}</td>
										<td>{user.phone_num}</td>
										<td><Button href={"/edit-user/" + user.uid} bsStyle="info">Edit</Button></td>
										<td><Button type="submit" onClick={this.deleteUser.bind(this, user.uid)} bsStyle="danger">Delete</Button></td>
									</tr>
								)}
							</tbody>
						</Table>
		          	</Row>
	          	</Grid>
	      	</div>
	    );
  }
}

export default UsersListComponent;