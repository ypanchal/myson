import {Navbar, Nav, NavItem, NavDropdown, MenuItem, Image} from 'react-bootstrap';
import { Link } from "react-router-dom";
import React from 'react';

export const adminNavbar = () => {
	return (<Nav>
				<NavDropdown eventKey={1} title="User" id="user-nav-dropdown">
				<MenuItem eventKey={1.1}><Link to="/users">Users</Link></MenuItem>
				<MenuItem eventKey={1.2}><Link to="/add-user">Add User</Link></MenuItem>
				</NavDropdown>
				<NavDropdown eventKey={2} title="Group" id="basic-nav-dropdown">
		        <MenuItem eventKey={2.1}><Link to="/groups">Groups</Link></MenuItem>
		        <MenuItem eventKey={2.2}><Link to="/add-group">Add Group</Link></MenuItem>
				</NavDropdown>
				<NavDropdown eventKey={4} title="Servers" id="user-nav-dropdown">
				<MenuItem eventKey={4.1}><Link to="/servers">Servers</Link></MenuItem>
				<MenuItem eventKey={4.2}><Link to="/server-add">Add Server</Link></MenuItem>
				</NavDropdown>
				<NavDropdown eventKey={3} title="Actions" id="basic-nav-dropdown">
		        <MenuItem eventKey={3.1}><Link to="/send-message">Send Message</Link></MenuItem>
		        <MenuItem eventKey={3.2}><Link to="/send-image">Send Image</Link></MenuItem>
		        <MenuItem eventKey={3.3}><Link to="/send-document">Send Document</Link></MenuItem>
				</NavDropdown>
			</Nav>
		);
}

export const normalNavbar = () => {
    return (<Nav>
				<NavDropdown eventKey={3} title="Actions" id="basic-nav-dropdown">
		        <MenuItem eventKey={3.1}><Link to="/send-message">Send Message</Link></MenuItem>
		        <MenuItem eventKey={3.2}><Link to="/send-image">Send Image</Link></MenuItem>
		        <MenuItem eventKey={3.3}><Link to="/send-document">Send Document</Link></MenuItem>
				</NavDropdown>
			</Nav>
		);
}