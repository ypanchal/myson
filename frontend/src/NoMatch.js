import React, { Component }  from 'react';

class NoMatch extends Component {

	render() {
		return (
			<div>
				<center><h1>404 Page Not Found</h1></center>
			</div>
		)
	}
}

export default NoMatch;