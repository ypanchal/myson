import React, { Component }  from 'react';
import {Grid, Row, Form, FormGroup, FormControl, Col, ControlLabel, controlId, Checkbox, Button, Alert, InputGroup, HelpBlock} from 'react-bootstrap';
import Spinner from 'react-spinkit';
import {loginRequired, adminRequired} from './LoginRequiredMixin.js';


class UsersEditComponent extends Component {
	constructor(props) {
		super(props);
	    this.state = {
			uid: {"value": "", "is_valid": null, "error": ""},
			phone_num: {"value": "", "is_valid": null, "error": ""},
			first_name: {"value": "", "is_valid": null, "error": ""},
			last_name: {"value": "", "is_valid": null, "error": ""},
			username: {"value": "", "is_valid": null, "error": ""},
			password: {"value": "", "is_valid": null, "error": ""},
			cnfpassword: {"value": "", "is_valid": null, "error": ""},
			is_admin: {"value": "", "is_valid": null, "error": ""},
			is_password_update: {"value": "", "is_valid": null, "error": ""},
			msg: {"value": false},
			is_disabled: {"value": true},
			is_loading: false,
			is_hidden: "form-group hidden"
		};
	}

	fetchUser() {
		var that = this;
		var url = "http://localhost:5000/api/user/" + that.props.match.params.uid + "/"
		fetch(url).then(function (response) {
			return response.json();
		}).then(function (result) {
			that.setState({
				uid: {"value": result.user.uid, "is_valid": null, "error": ""},
				phone_num: {"value": result.user.phone_num, "is_valid": null, "error": ""},
				first_name: {"value": result.user.first_name, "is_valid": null, "error": ""},
				last_name: {"value": result.user.last_name, "is_valid": null, "error": ""},
				username: {"value": result.user.username, "is_valid": null, "error": ""},
				is_admin: {"value": result.user.is_admin, "is_valid": null, "error": ""},
				is_password_update: {"value": undefined, "is_valid": null, "error": ""},
				msg: {"value": false},
				is_disabled: {"value": true},
				is_loading: false,
				is_hidden: "form-group hidden"
			})
		});
	}

	handleDismiss = (event) => {
		const state = this.state
		state["msg"]["value"] = false;
		this.setState(state);
	}

	showPasswordField = (event) => {
		const state = this.state;
		if (event.target.checked){
			state["is_hidden"] = "form-group";
			state["is_password_update"]["value"] = event.target.checked;
			this.setState(state);
		} else {
			state["is_hidden"] = "form-group hidden";
			state["password"]["value"] = "";
			state["cnfpassword"]["value"] = "";
			state["is_password_update"]["value"] = event.target.checked;
			state["password"]["error"] = "";
			state["cnfpassword"]["error"] = "";
			state["password"]["is_valid"] = null;
			state["cnfpassword"]["is_valid"] = null;
			this.setState(state);
		}
	}

	onChange = (event) => {
		const state = this.state
		const target = event.target;
		const name = target.name;
		const value = target.type === 'checkbox' ? target.checked : target.value;
		state[name]["value"] = value;
		if (name !== "is_admin" && value === ""){
			state[name]["is_valid"] = "error";
			state[name]["error"] = "This Field is Required.";
		} else {
			state[name]["value"] = value;
			state[name]["is_valid"] = "success";
			state[name]["error"] = "";
		}

		if (state.first_name.value !== "" && state.last_name.value !== "" && state.username.value !== "" && state.phone_num.value !== ""){
			state["is_disabled"]["value"] = false;
		} else {
			state["is_disabled"]["value"] = true;
		}

		if (state.password.value !== "" && state.cnfpassword.value !== ""){
			if (state.password.value.length < 6){
				state["is_disabled"]["value"] = true;
				state["password"]["is_valid"] = "error";
				state["password"]["error"] = "minimum password length should be 6 characters";
			} else {
				if (state.password.value !== state.cnfpassword.value){
					state["is_disabled"]["value"] = true;
					state["password"]["is_valid"] = "error";
					state["password"]["error"] = "password & confirm password does not match";
				} else {
					state["is_disabled"]["value"] = false;
					state["password"]["is_valid"] = "success";
					state["password"]["error"] = "";
				}
			}
		}

		this.setState(state);
	}

	handleOnSubmit = (event) => {
		event.preventDefault();
		const state = this.state;

		const data = {
			uid: state.uid.value,
			phone_num: state.phone_num.value,
			first_name: state.first_name.value,
			last_name: state.last_name.value,
			username: state.username.value,
			is_admin: state.is_admin.value,
			password: state.password.value,
			cnfpassword: state.cnfpassword.value,
			is_password_update: state.is_password_update.value
		}
		fetch('http://localhost:5000/api/edit-user/', {
			headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
			method: 'POST',
			body: JSON.stringify(data),
		}).then(res => {
			if(res.status === 200) {
				const new_state = state;
				new_state["uid"]["is_valid"] = null;
				new_state["first_name"]["is_valid"] = null;
				new_state["last_name"]["is_valid"] = null;
				new_state["username"]["is_valid"] = null;
				new_state["password"]["is_valid"] = null;
				new_state["cnfpassword"]["is_valid"] = null;
				new_state["is_password_update"]["is_valid"] = null;
				new_state["phone_num"]["is_valid"] = null;
				new_state["is_admin"]["is_valid"] = null;
				new_state["uid"]["error"] = "";
				new_state["first_name"]["error"] = "";
				new_state["last_name"]["error"] = "";
				new_state["username"]["error"] = "";
				new_state["phone_num"]["error"] = "";
				new_state["is_admin"]["error"] = "";
				new_state["password"]["error"] = "";
				new_state["cnfpassword"]["error"] = "";
				new_state["is_password_update"]["error"] = "";
				new_state["is_password_update"]["value"] = false;
				new_state["is_hidden"] = "form-group hidden"
				new_state["is_disabled"]["value"] = true;
				new_state["msg"]["value"] = true;
				this.setState(new_state);
			} else {
				this.setState(state);
			};
		});
	}

	componentWillMount() {
		loginRequired();
		adminRequired();
		this.fetchUser("reactjs");
	}
	render() {
		const is_msg = this.state.msg.value;
  		const is_loading = this.state.is_loading;
		return (
		  <div>
		    <Grid>
		  		<Row>
		  			<Col xs={6} md={5} lg={6} mdOffset={3}>
		  				<h2>User Edit</h2>
		  				{ is_msg &&
		      				<Alert bsStyle="success" onDismiss={this.handleDismiss}>
		      					<p>
		      						User Updated Successfully.
		      					</p>
					        </Alert>
				    	}
			    		<Form horizontal onSubmit={this.handleOnSubmit}>
			    			<FormGroup controlId="formHorizontalUID" validationState={ this.state.uid.is_valid }>
			    				<Col componentClass={ControlLabel} sm={2}>
						      		UID
						      	</Col>
						      	<Col sm={10}>
						      		<FormControl inline="true" type="Text" placeholder="UID" onChange={this.onChange} name="uid" value={ this.state.uid.value } readOnly="true" />
						      		<FormControl.Feedback />
						      		<HelpBlock>{ this.state.uid.error }</HelpBlock>
						      		{ is_loading &&
						      			<Spinner name="three-bounce" />
						      		}
						    	</Col>
						    </FormGroup>
						    <FormGroup controlId="formHorizontalPHONENUM" validationState={ this.state.phone_num.is_valid }>
						    	<Col componentClass={ControlLabel} sm={2}>Phone Number
						    	</Col>
						    	<Col sm={10}>
						    		<InputGroup>
		  							<InputGroup.Addon>+91</InputGroup.Addon>
						    		<FormControl type="Text" placeholder="Phone Number" onChange={this.onChange} name="phone_num" value={ this.state.phone_num.value } />
						    		<FormControl.Feedback />
						    		</InputGroup>
						    		<HelpBlock>{ this.state.phone_num.error }</HelpBlock>
						    	</Col>
						    </FormGroup>
						    <FormGroup controlId="formHorizontalFNAME" validationState={ this.state.first_name.is_valid }>
						    	<Col componentClass={ControlLabel} sm={2}>First Name
						    	</Col>
						    	<Col sm={10}>
						    		<FormControl type="Text" placeholder="First Name" onChange={this.onChange} name="first_name" value={ this.state.first_name.value } />
						    		<FormControl.Feedback />
						    		<HelpBlock>{ this.state.first_name.error }</HelpBlock>
						    	</Col>
						    </FormGroup>
						    <FormGroup controlId="formHorizontalLNAME" validationState={ this.state.last_name.is_valid }>
						    	<Col componentClass={ControlLabel} sm={2}>Last Name
						    	</Col>
						    	<Col sm={10}>
						    		<FormControl type="Text" placeholder="Last Name" onChange={this.onChange} name="last_name" value={ this.state.last_name.value } />
						    		<FormControl.Feedback />
						    		<HelpBlock>{ this.state.last_name.error }</HelpBlock>
						    	</Col>
						    </FormGroup>
						    <FormGroup controlId="formHorizontalUNAME" validationState={ this.state.username.is_valid }>
						    	<Col componentClass={ControlLabel} sm={2}>Username
						    	</Col>
						    	<Col sm={10}>
						    		<FormControl type="Text" placeholder="Username" onChange={this.onChange} name="username" value={ this.state.username.value } />
						    		<FormControl.Feedback />
						    		<HelpBlock>{ this.state.username.error }</HelpBlock>
						    	</Col>
						    </FormGroup>
						    <FormGroup bsClass={this.state.is_hidden} controlId="formHorizontalPASS" validationState={ this.state.password.is_valid }>
					    		<Col componentClass={ControlLabel} sm={2}>Password
					    		</Col>
					    		<Col sm={10}>
					    			<FormControl type="Password" placeholder="password" onChange={this.onChange} name="password" value={ this.state.password.value } />
					    			<FormControl.Feedback />
					    			<HelpBlock>{ this.state.password.error }</HelpBlock>
					    		</Col>
					    	</FormGroup>
					    	<FormGroup bsClass={this.state.is_hidden} controlId="formHorizontalCNFPASS" validationState={ this.state.cnfpassword.is_valid }>
					    		<Col componentClass={ControlLabel} sm={2}>Confirm Password
					    		</Col>
					    		<Col sm={10}>
					    			<FormControl type="Password" placeholder="Confirm Password" onChange={this.onChange} name="cnfpassword" value={ this.state.cnfpassword.value } />
					    			<FormControl.Feedback />
					    			<HelpBlock>{ this.state.cnfpassword.error }</HelpBlock>
					    		</Col>
					    	</FormGroup>
					    	<FormGroup controlId="formHorizontalISUPDATE">
						    	<Col componentClass={ControlLabel} sm={2}>Change Password
						    	</Col>
						    	<Col sm={2}>
						    		<Checkbox name="is_password_update" checked={this.state.is_password_update.value} value={this.state.is_password_update.value} onChange={this.showPasswordField}></Checkbox>
						    	</Col>
						    </FormGroup>
						    <FormGroup controlId="formHorizontalISADMIN">
						    	<Col componentClass={ControlLabel} sm={2}>Is Admin
						    	</Col>
						    	<Col sm={2}>
						    		<Checkbox name="is_admin" checked={this.state.is_admin.value} value={this.state.is_admin.value} onChange={this.onChange}></Checkbox>
						    	</Col>
						    </FormGroup>
						    <FormGroup>
						    	<Col sm={12}>
						    		<Button bsStyle="primary" disabled={this.state.is_disabled.value} type="submit">Submit</Button>
						    	</Col>
						    </FormGroup>
						</Form>
					</Col>
				</Row>
			</Grid>
		  </div>
		);
	}
}

export default UsersEditComponent;