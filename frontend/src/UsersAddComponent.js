import React, { Component }  from 'react';
import {Grid, Row, Form, FormGroup, FormControl, Col, ControlLabel, controlId, Checkbox, Button, Alert, InputGroup, HelpBlock} from 'react-bootstrap';
import Spinner from 'react-spinkit';
import {loginRequired, adminRequired} from './LoginRequiredMixin.js';


function getInitialState(){
	return {
		uid: {"value": "", "is_valid": null, "error": ""},
		phone_num: {"value": "", "is_valid": null, "error": ""},
		first_name: {"value": "", "is_valid": null, "error": ""},
		last_name: {"value": "", "is_valid": null, "error": ""},
		username: {"value": "", "is_valid": null, "error": ""},
		password: {"value": "", "is_valid": null, "error": ""},
		cnfpassword: {"value": "", "is_valid": null, "error": ""},
		is_admin: {"value": false, "is_valid": null, "error": ""},
		msg: {"value": false},
		is_disabled: {"value": true},
		is_loading: false
	};
}


class UsersAddComponent extends Component {
	constructor(){
		super();
		this.state = getInitialState();
	}

	handleDismiss = (event) => {
		const state = this.state
		state["msg"]["value"] = false;
		this.setState(state);
	}

	fetchUid = (event) => {
		event.preventDefault();
		const that = this
		const state = that.state;
		state["is_loading"] = true;
		state["uid"]["error"] = "";
		state["uid"]["is_valid"] = null;
		state["is_disabled"]["value"] = true;
		that.setState(state);
		const data = {
			phone_num: state.phone_num.value,
			first_name: state.first_name.value,
			last_name: state.last_name.value
		}
		fetch('http://localhost:5000/api/get-uid', {
			headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
			method: 'POST',
			body: JSON.stringify(data),
		}).then(function (response) {
			return response.json();
		}).then(function (result) {
			const res = result.uid;
			if (res.error){
				state["is_loading"] = false;
				state["uid"]["error"] = res.msg;
				state["uid"]["is_valid"] = "error";
				that.setState(state);
			} else {
				state["uid"]["value"] = res.uid;
				state["uid"]["is_valid"] = "success";
				that.setState(state);
				that.handleOnSubmit();
			}
		});
	}

	onChange = (event) => {
		const state = this.state
		const target = event.target;
		const name = target.name;
		const value = target.type === 'checkbox' ? target.checked : target.value;
		state[name]["value"] = value;
		if (name !== "is_admin" && value === ""){
			state[name]["is_valid"] = "error";
			state[name]["error"] = "This Field is Required.";
		} else {
			state[name]["value"] = value;
			state[name]["is_valid"] = "success";
			state[name]["error"] = "";
		}

		if (state.password.value !== "" && state.cnfpassword.value !== ""){
			if (state.password.value.length < 6){
				state["is_disabled"]["value"] = true;
				state["password"]["is_valid"] = "error";
				state["password"]["error"] = "minimum password length should be 6 characters";
			} else {
				if (state.password.value !== state.cnfpassword.value){
					state["is_disabled"]["value"] = true;
					state["password"]["is_valid"] = "error";
					state["password"]["error"] = "password & confirm password does not match";
				} else {
					state["is_disabled"]["value"] = false;
					state["password"]["is_valid"] = "success";
					state["password"]["error"] = "";
				}
			}
		}

		if (state.first_name.value !== "" && state.last_name.value !== "" && state.username.value !== "" && state.phone_num.value !== "" && state.password.value !== "" && state.cnfpassword.value !== ""){
			state["is_disabled"]["value"] = false;
		} else {
			state["is_disabled"]["value"] = true;
		}

		this.setState(state);
	}

	handleOnSubmit = () => {
		const state = this.state;
		if (state["uid"]["is_valid"] === "error"){
			state["is_loading"] = false;
			return false;
		}

		const data = {
			uid: state.uid.value,
			phone_num: state.phone_num.value,
			first_name: state.first_name.value,
			last_name: state.last_name.value,
			username: state.username.value,
			password: state.password.value,
			cnfpassword: state.cnfpassword.value,
			is_admin: state.is_admin.value,
		}
		fetch('http://localhost:5000/api/create-user/', {
			headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
			method: 'POST',
			body: JSON.stringify(data),
		}).then(res => {
			if(res.status === 200) {
				const new_state = getInitialState();
				new_state["msg"]["value"] = true;
				this.setState(new_state);
			} else {
				this.setState(getInitialState());
			};
		});
	}

	componentWillMount() {
		loginRequired();
		adminRequired();
	}

  render() {
  	const is_msg = this.state.msg.value;
  	const is_loading = this.state.is_loading;
    return (
      <div>
        <Grid>
      		<Row>
      			<Col xs={6} md={5} lg={6} mdOffset={3}>
      				<h2>User Add</h2>
      				{ is_msg &&
	      				<Alert bsStyle="success" onDismiss={this.handleDismiss}>
	      					<p>
	      						User Created Successfully.
	      					</p>
				        </Alert>
			    	}
		    		<Form horizontal onSubmit={this.fetchUid}>
		    			<FormGroup controlId="formHorizontalUID" validationState={ this.state.uid.is_valid }>
		    				<Col componentClass={ControlLabel} sm={2}>
					      		UID
					      	</Col>
					      	<Col sm={10}>
					      		<FormControl inline="true" type="Text" placeholder="UID" onChange={this.onChange} name="uid" value={ this.state.uid.value } readonly="true" />
					      		<FormControl.Feedback />
					      		<HelpBlock>{ this.state.uid.error }</HelpBlock>
					      		<Row>
					      		{ is_loading &&
					      			<Spinner name="three-bounce" />
					      		}
					      		</Row>
					    	</Col>
					    </FormGroup>
					    <FormGroup controlId="formHorizontalPHONENUM" validationState={ this.state.phone_num.is_valid }>
					    	<Col componentClass={ControlLabel} sm={2}>Phone Number
					    	</Col>
					    	<Col sm={10}>
					    		<InputGroup>
      							<InputGroup.Addon>+91</InputGroup.Addon>
					    		<FormControl type="Text" placeholder="Phone Number" onChange={this.onChange} name="phone_num" value={ this.state.phone_num.value } />
					    		<FormControl.Feedback />
					    		</InputGroup>
					    		<HelpBlock>{ this.state.phone_num.error }</HelpBlock>
					    	</Col>
					    </FormGroup>
					    <FormGroup controlId="formHorizontalFNAME" validationState={ this.state.first_name.is_valid }>
					    	<Col componentClass={ControlLabel} sm={2}>First Name
					    	</Col>
					    	<Col sm={10}>
					    		<FormControl type="Text" placeholder="First Name" onChange={this.onChange} name="first_name" value={ this.state.first_name.value } />
					    		<FormControl.Feedback />
					    		<HelpBlock>{ this.state.first_name.error }</HelpBlock>
					    	</Col>
					    </FormGroup>
					    <FormGroup controlId="formHorizontalLNAME" validationState={ this.state.last_name.is_valid }>
					    	<Col componentClass={ControlLabel} sm={2}>Last Name
					    	</Col>
					    	<Col sm={10}>
					    		<FormControl type="Text" placeholder="Last Name" onChange={this.onChange} name="last_name" value={ this.state.last_name.value } />
					    		<FormControl.Feedback />
					    		<HelpBlock>{ this.state.last_name.error }</HelpBlock>
					    	</Col>
					    </FormGroup>
					    <FormGroup controlId="formHorizontalUNAME" validationState={ this.state.username.is_valid }>
					    	<Col componentClass={ControlLabel} sm={2}>Username
					    	</Col>
					    	<Col sm={10}>
					    		<FormControl type="Text" placeholder="Username" onChange={this.onChange} name="username" value={ this.state.username.value } />
					    		<FormControl.Feedback />
					    		<HelpBlock>{ this.state.username.error }</HelpBlock>
					    	</Col>
					    </FormGroup>
					    <FormGroup controlId="formHorizontalPASS" validationState={ this.state.password.is_valid }>
					    	<Col componentClass={ControlLabel} sm={2}>password
					    	</Col>
					    	<Col sm={10}>
					    		<FormControl type="Password" placeholder="password" onChange={this.onChange} name="password" value={ this.state.password.value } />
					    		<FormControl.Feedback />
					    		<HelpBlock>{ this.state.password.error }</HelpBlock>
					    	</Col>
					    </FormGroup>
					    <FormGroup controlId="formHorizontalCNFPASS" validationState={ this.state.cnfpassword.is_valid }>
					    	<Col componentClass={ControlLabel} sm={2}>cnfpassword
					    	</Col>
					    	<Col sm={10}>
					    		<FormControl type="Password" placeholder="Confirm Password" onChange={this.onChange} name="cnfpassword" value={ this.state.cnfpassword.value } />
					    		<FormControl.Feedback />
					    		<HelpBlock>{ this.state.cnfpassword.error }</HelpBlock>
					    	</Col>
					    </FormGroup>
					    <FormGroup controlId="formHorizontalISADMIN">
					    	<Col componentClass={ControlLabel} sm={2}>Is Admin
					    	</Col>
					    	<Col sm={2}>
					    		<Checkbox name="is_admin" checked={this.state.is_admin.value} value={this.state.is_admin.value} onChange={this.onChange}></Checkbox>
					    	</Col>
					    </FormGroup>
					    <FormGroup>
					    	<Col sm={12}>
					    		<Button bsStyle="primary" disabled={this.state.is_disabled.value} type="submit">Submit</Button>
					    	</Col>
					    </FormGroup>
					</Form>
				</Col>
			</Row>
		</Grid>
      </div>
    );
  }
}

export default UsersAddComponent;