import React, { Component }  from 'react';
import {Grid, Row, Form, FormGroup, FormControl, Col, ControlLabel, controlId, Checkbox, Button, Alert, HelpBlock} from 'react-bootstrap';
import Spinner from 'react-spinkit';
import ReactDOM from 'react-dom';
import {loginRequired} from './LoginRequiredMixin.js';


function getInitialState(){
	return {
    	username: {"value": "", "is_valid": null, "error": ""},
		send_to: {"value": [], "is_valid": null, "error": ""},
		message: {"value": "", "is_valid": null, "error": ""},
		is_disabled: {"value": true},
    	msg: {"value": false}
    };
}


class SendMessageComponent extends Component {

	constructor(props) {
		super(props);

	    this.state = {
	    	users: [],
	    	username: {"value": "yspanchal", "is_valid": null, "error": ""},
			send_to: {"value": "", "is_valid": null, "error": ""},
			message: {"value": "", "is_valid": null, "error": ""},
			is_disabled: {"value": true},
	    	msg: {"value": false}
	    };
	}

	handleDismiss = (event) => {
		const state = this.state
		state["msg"] = false;
		this.setState(state);
	}

	fetchUsers() {
		var that = this;
		fetch("http://localhost:5000/api/users-list/").then(function (response) {
			return response.json();
		}).then(function (result) {
			var new_state = that.state
			new_state["users"] = result.users;
			that.setState(new_state);
		});
	}

	onChange = (event) => {
		const state = this.state
		const target = event.target;
		const name = target.name;
		var value = target.value;

		state[name]["value"] = value;

		if (value === "" || value === "select"){
			state[name]["is_valid"] = "error";
			state[name]["error"] = "This Field is Required.";
		} else {
			state[name]["is_valid"] = "success";
			state[name]["error"] = "";
		}

		if (state.send_to.value !== "" && state.send_to.value !== "select" && state.message.value !== ""){
			state["is_disabled"]["value"] = false;
		} else {
			state["is_disabled"]["value"] = true;
		}
		this.setState(state);
	}

	handleOnSubmit = (event) => {
		event.preventDefault();
		const that = this;
		const state = that.state;
		state["is_loading"] = true;
		state["is_disabled"]["value"] = true;
		that.setState(state);

		const data = {
			send_to: state.send_to.value,
			message: state.message.value,
			username: state.username.value
		}
		fetch('http://localhost:5000/api/notify/', {
			headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
			method: 'POST',
			body: JSON.stringify(data),
		}).then(res => {
			if(res.status === 200) {
				const new_state = getInitialState();
				new_state["msg"]["value"] = true;
				new_state["is_loading"] = false;
				new_state["users"] = state.users;
				new_state["username"] = state.username;
				that.setState(new_state);
				ReactDOM.findDOMNode(this.refs.send_to).value = 'select';
			} else {
				that.setState(getInitialState());
			};
		});
	}

	componentWillMount() {
		loginRequired();
		this.fetchUsers("reactjs");
	}

  render() {
  	const is_msg = this.state.msg.value;
  	const is_loading = this.state.is_loading;
    return (
		<div>
	        <Grid>
	      		<Row>
	      			<Col xs={6} md={5} lg={6} mdOffset={3}>
	      				<h2>Send Message</h2>
	      				{ is_msg &&
		      				<Alert bsStyle="success" onDismiss={this.handleDismiss}>
		      					<p>
		      						Message sent Successfully.
		      					</p>
					        </Alert>
				    	}
			    		<Form horizontal onSubmit={this.handleOnSubmit}>
			    			<FormGroup controlId="formHorizontalUsername" validationState={ this.state.username.is_valid }>
			    				<Col componentClass={ControlLabel} sm={2}>
						      		Username
						      	</Col>
						      	<Col sm={10}>
						      		<FormControl type="text" placeholder="Username" onChange={this.onChange} name="group_name" value={ this.state.username.value } readOnly="true" />
						      		<FormControl.Feedback />
						      		<HelpBlock>{ this.state.username.error }</HelpBlock>
						      		{ is_loading &&
						      			<Spinner name="three-bounce" />
						      		}
						    	</Col>
						    </FormGroup>
						    <FormGroup controlId="formHorizontalSTO" validationState={ this.state.send_to.is_valid }>
						    	<Col componentClass={ControlLabel}  sm={2}>
						    		Send To
						    	</Col>
						    	<Col sm={10}>
						    		<FormControl componentClass="select" placeholder="select" onChange={this.onChange} name="send_to" ref="send_to">
						    			<option value="select">select</option>
						    			{
					    				this.state.users.map((user, index) => 
					    					<option value={user.id}>{user.name}</option>
					    				)}
							      	</FormControl>
						    		<FormControl.Feedback />
						      		<HelpBlock>{ this.state.send_to.error }</HelpBlock>
						      	</Col>
						    </FormGroup>
						    <FormGroup controlId="formHorizontalMessage" validationState={ this.state.message.is_valid }>
						    	<Col componentClass={ControlLabel} sm={2}>Message
						    	</Col>
						    	<Col sm={10}>
						    		<FormControl componentClass="textarea" placeholder="Message"  onChange={this.onChange} name="message" value={ this.state.message.value } />
						    		<FormControl.Feedback />
						      		<HelpBlock>{ this.state.message.error }</HelpBlock>
						    	</Col>
						    </FormGroup>
						    <FormGroup>
						    	<Col sm={12}>
						    		<Button bsStyle="primary" disabled={this.state.is_disabled.value} type="submit">Submit</Button>
						    	</Col>
						    </FormGroup>
						</Form>
					</Col>
				</Row>
			</Grid>
        </div>
    );
  }
}

export default SendMessageComponent;