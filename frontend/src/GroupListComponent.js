import React, { Component }  from 'react';
import {Grid, Row, Table, Button} from 'react-bootstrap';
import {loginRequired, adminRequired} from './LoginRequiredMixin.js';


class GroupListComponent extends Component {

	constructor(props) {
		super(props);

	    this.state = {
	    	groups: []
	    };
	}

	fetchGroups() {
		var that = this;
		fetch("http://localhost:5000/api/groups/").then(function (response) {
			return response.json();
		}).then(function (result) {
			console.log(result.groups);
			that.setState({groups: result.groups});
		});
	}

	componentWillMount() {
		loginRequired();
		adminRequired();
		this.fetchGroups("reactjs");
	}

	titleCase(str) {
	  str = str.toLowerCase().split(' ');
	  for (var i = 0; i < str.length; i++) {
	    str[i] = str[i].charAt(0).toUpperCase() + str[i].slice(1); 
	  }
	  return str.join(' ');
	}


  render() {
    return (
        <div>
    		<Grid>
	    		<Row>
	          		<h2>Groups</h2>
	          		<Table striped bordered condensed hover>
	          			<thead>
		          			<tr>
								<th>#</th>
								<th>GID</th>
								<th>Group Name</th>
								<th>Members</th>
							</tr>
						</thead>
						<tbody>
						{this.state.groups.map((group, index) =>
							<tr>
								<td>{index+1}</td>
								<td>{group.gid}</td>
								<td>{group.group_name}</td>
								<td>{group.members.map((member, index) =>
									group.members.length > index+1 ? this.titleCase(member) + ", " : this.titleCase(member)
									)}</td>
							</tr>
						)}
						</tbody>
					</Table>
	          	</Row>
          	</Grid>
      	</div>
    );
  }
}

export default GroupListComponent;