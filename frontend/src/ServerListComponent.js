import React, { Component }  from 'react';
import {Grid, Row, Table, Button} from 'react-bootstrap';
import {loginRequired, adminRequired} from './LoginRequiredMixin.js';


class ServerListComponent extends Component {

	constructor(props) {
		super(props);

	    this.state = {
	    	servers: []
	    };
	}

	fetchServers() {
		var that = this;
		fetch("http://localhost:5000/api/servers/").then(function (response) {
			return response.json();
		}).then(function (result) {
			console.log(result.servers);
			that.setState({servers: result.servers});
		});
	}

	componentWillMount() {
		loginRequired();
		adminRequired();
		this.fetchServers("reactjs");
	}

	titleCase(str) {
	  str = str.toLowerCase().split(' ');
	  for (var i = 0; i < str.length; i++) {
	    str[i] = str[i].charAt(0).toUpperCase() + str[i].slice(1); 
	  }
	  return str.join(' ');
	}


  render() {
    return (
        <div>
    		<Grid>
	    		<Row>
	          		<h2>Servers</h2>
	          		<Table striped bordered condensed hover>
	          			<thead>
		          			<tr>
								<th>#</th>
								<th>Server Name</th>
								<th>Server IP</th>
								<th>Server Username</th>
								<th>Server Password</th>
								<th>Server Access</th>
							</tr>
						</thead>
						<tbody>
						{this.state.servers.map((server, index) =>
							<tr>
								<td>{index+1}</td>
								<td>{server.server_name}</td>
								<td>{server.server_ip}</td>
								<td>{server.server_username}</td>
								<td>******</td>
								<td>{server.users.map((user, index) =>
									server.users.length > index+1 ? this.titleCase(user) + ", " : this.titleCase(user)
									)}</td>
							</tr>
						)}
						</tbody>
					</Table>
	          	</Row>
          	</Grid>
      	</div>
    );
  }
}

export default ServerListComponent;
