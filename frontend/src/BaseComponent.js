import React, { Component }  from 'react';
import './index.css';
import {Navbar, Nav, NavItem, NavDropdown, MenuItem, Image} from 'react-bootstrap';
import { Switch, Route, Link, BrowserRouter as Router } from "react-router-dom";
import LoginComponent from './LoginComponent';
import UsersListComponent from './UsersListComponent';
import UsersAddComponent from './UsersAddComponent';
import UsersEditComponent from './UsersEditComponent';
import UsersDeleteComponent from './UsersDeleteComponent';
import GroupListComponent from './GroupListComponent';
import GroupAddComponent from './GroupAddComponent';
import SendImageComponent from './SendImageComponent';
import SendDocumentComponent from './SendDocumentComponent';
import SendMessageComponent from './SendMessageComponent';
import ServerAddComponent from './ServerAddComponent';
import ServerListComponent from './ServerListComponent';
import LogoutComponent from './LogoutComponent';
import NoMatch from './NoMatch';
import {adminNavbar, normalNavbar} from './NavbarComponent';


class BaseComponent extends Component {

	constructor(props) {
		super(props);

	    this.state = {
	    	is_loggedin: localStorage.token,
	    	is_admin: localStorage.is_admin
	    };
	}

	onselect = (event) => {
		
	}

	render() {
		const token = this.state.is_loggedin;
		const is_admin = this.state.is_admin;
		var navbar;
		if (token){
			if (is_admin === "true"){
				navbar = adminNavbar();
			} else {
				navbar = normalNavbar();
			}
		} else {
			navbar = null;
		}
		return (
			<div>
	    		<Navbar inverse collapseOnSelect >
		  			<Navbar.Header>
			    		<Navbar.Brand>
			      			<Image src="/logo.png" href="/" responsive />
			    		</Navbar.Brand>
			    		<Navbar.Toggle />
		  			</Navbar.Header>
			  		<Navbar.Collapse>
			  			{navbar}
			    		<Nav pullRight>
			    			{token ? (
			    				<NavItem eventKey={1}>
				    				<Link to="/logout">
				    					Logout
				    				</Link>
			    				</NavItem>
			    			) : (
			    				<NavItem eventKey={1}>
			    					<Link to="/">
			    						Login
			    					</Link>
			    				</NavItem>
			    			)
			    			}
			    		</Nav>
			  		</Navbar.Collapse>
				</Navbar>
				<Switch>
					<Route exact path="/" component={LoginComponent} />
					<Route exact path="/users" component={UsersListComponent} />
					<Route exact path="/add-user" component={UsersAddComponent} />
					<Route exact path="/edit-user/:uid" component={UsersEditComponent} />
					<Route exact path="/delete-user/:uid" component={UsersDeleteComponent} />
					<Route exact path="/groups" component={GroupListComponent} />
					<Route exact path="/add-group" component={GroupAddComponent} />
					<Route exact path="/send-message" component={SendMessageComponent} />
					<Route exact path="/send-image" component={SendImageComponent} />
					<Route exact path="/send-document" component={SendDocumentComponent} />
					<Route exact path="/server-add" component={ServerAddComponent} />
					<Route exact path="/servers" component={ServerListComponent} />
					<Route exact path="/logout" component={LogoutComponent} />
					<Route component={NoMatch} />
				</Switch>
			</div>
		);
	}
}

export default BaseComponent;
