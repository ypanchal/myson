import React, { Component }  from 'react';
import './index.css';
import {Grid, Row, Form, FormGroup, FormControl, Col, ControlLabel, controlId, Checkbox, Button, Panel, Alert, HelpBlock} from 'react-bootstrap';
import Spinner from 'react-spinkit';


function getInitialState(){
	return {
		username: {"value": "", "is_valid": null, "error": ""},
		password: {"value": "", "is_valid": null, "error": ""},
		is_disabled: {"value": true},
		msg: {"value": false}
	};
}


class LoginComponent extends Component {

	constructor(props) {
		super(props);

	    this.state = {
	    	l: this.props.l,
	    	username: {"value": "", "is_valid": null, "error": ""},
			password: {"value": "", "is_valid": null, "error": ""},
			is_disabled: {"value": true},
	    	msg: {"value": false}
	    };
	}

	handleDismiss = (event) => {
		const state = this.state
		state["msg"]["value"] = false;
		this.setState(state);
	}

	fetchUsers() {
		var that = this;
		fetch("http://localhost:5000/auth/login/").then(function (response) {
			return response.json();
		}).then(function (result) {
			var new_state = that.state;
			that.setState(new_state);
		});
	}

	onChange = (event) => {
		const state = this.state
		const target = event.target;
		const name = target.name;
		var value = target.value;

		state[name]["value"] = value;

		if (value === ""){
			state[name]["is_valid"] = "error";
			state[name]["error"] = "This Field is Required.";
		} else {
			state[name]["is_valid"] = "success";
			state[name]["error"] = "";
		}

		if (state.username.value !== "" && state.password.value !== ""){
			state["is_disabled"]["value"] = false;
		} else {
			state["is_disabled"]["value"] = true;
		}
		this.setState(state);
	}

	setData = (data) => {
		localStorage.setItem('token', data.auth_token);
		localStorage.setItem('username', data.username);
		localStorage.setItem('uid', data.uid);
		localStorage.setItem('is_admin', data.is_admin);
	}

	handleOnSubmit = (event) => {
		event.preventDefault();
		const that = this;
		const state = that.state;
		const data = {
			username: state.username.value,
			password: state.password.value
		}
		fetch('http://localhost:5000/auth/login/', {
			headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
			method: 'POST',
			body: JSON.stringify(data),
		}).then(function (response) {
			return response.json();
		}).then(function (result) {
			if (result.error){
				state["is_loading"] = false;
				state["username"]["error"] = result.error;
				state["username"]["is_valid"] = "error";
				that.setState(state);
			} else {
				that.setData(result);
				var new_state = getInitialState();
				new_state["msg"]["value"] = true;
				that.setState(new_state);
				setTimeout(function() {
					if (result.is_admin) {
						window.location.href = "/users";
					} else {
						window.location.href = "/send-message";
					}
				}.bind(this), 1000)
			}
		});
	}

	componentWillMount(){
		var is_loggedin = localStorage.token;
		if (is_loggedin){
			window.location.href = "/users";
		}
	}

	render() {
		const is_msg = this.state.msg.value;
  		const is_loading = this.state.is_loading;
		return (
		  <div className="login-form">
		  	<Grid>
		  		<Row>
		  			<Col xs={6} md={5} mdOffset={4}>{ is_msg &&
		      				<Alert bsStyle="success" onDismiss={this.handleDismiss}>
		      					<p>
		      						Login Successfully.
		      					</p>
					        </Alert>
				    	}
		      			<Panel bsStyle="primary">
		      				<Panel.Heading>
		      					<Panel.Title componentClass="h3">Login</Panel.Title>
		      				</Panel.Heading>
						    <Panel.Body>		    	
					    		<Form horizontal onSubmit={this.handleOnSubmit}>
					    			<FormGroup controlId="formHorizontalusername" validationState={ this.state.username.is_valid }>
					    				<Col componentClass={ControlLabel} sm={2}>
								      		Email
								      	</Col>
								      	<Col sm={10}>
								      		<FormControl type="username" placeholder="Username"   onChange={this.onChange} name="username" value={ this.state.username.value } />
								      		<FormControl.Feedback />
								      		<HelpBlock>{ this.state.username.error }</HelpBlock>
								      		{ is_loading &&
								      			<Spinner name="three-bounce" />
								      		}
								    	</Col>
								    </FormGroup>
								    <FormGroup controlId="formHorizontalPassword"  validationState={ this.state.password.is_valid }>
								    	<Col componentClass={ControlLabel} sm={2}>Password
								    	</Col>
								    	<Col sm={10}>
								    		<FormControl type="password" placeholder="Password"  onChange={this.onChange} name="password" value={ this.state.password.value } />
								    		<FormControl.Feedback />
						      				<HelpBlock>{ this.state.password.error }</HelpBlock>
								    	</Col>
								    </FormGroup>
								    <FormGroup>
								    	<Col sm={12}>
								    		<Button type="submit">Sign In</Button>
								    	</Col>
								    </FormGroup>
								</Form>
					    	</Panel.Body>
						</Panel>
					</Col>
				</Row>
			</Grid>
		  </div>
		);
	}
}

export default LoginComponent;