import React, { Component }  from 'react';
import {Grid, Row, Form, FormGroup, FormControl, Col, ControlLabel, controlId, Checkbox, Button, HelpBlock, Alert} from 'react-bootstrap';
import Spinner from 'react-spinkit';
import ReactDOM from 'react-dom';
import {loginRequired, adminRequired} from './LoginRequiredMixin.js';


function getInitialState(){
	return {
    	users: [],
    	server_name: {"value": "", "is_valid": null, "error": ""},
    	server_ip: {"value": "", "is_valid": null, "error": ""},
    	server_username: {"value": "", "is_valid": null, "error": ""},
    	server_password: {"value": "", "is_valid": null, "error": ""},
		server_access: {"value": [], "is_valid": null, "error": ""},
		is_disabled: {"value": true},
    	msg: {"value": false},
    	is_loading: false
    };
}


class ServerAddComponent extends Component {

	constructor(props) {
		super(props);

	    this.state = {
	    	users: [],
	    	server_name: {"value": "", "is_valid": null, "error": ""},
	    	server_ip: {"value": "", "is_valid": null, "error": ""},
	    	server_username: {"value": "", "is_valid": null, "error": ""},
	    	server_password: {"value": "", "is_valid": null, "error": ""},
			server_access: {"value": [], "is_valid": null, "error": ""},
			is_disabled: {"value": true},
	    	msg: {"value": false},
	    	is_loading: false
	    };
	}

	handleDismiss = (event) => {
		const state = this.state
		state["msg"] = false;
		this.setState(state);
	}

	fetchUsers() {
		var that = this;
		fetch("http://localhost:5000/api/users/").then(function (response) {
			return response.json();
		}).then(function (result) {
			that.setState({users: result.users});
		});
	}

	getSelectedOptions(options) {
		var value = [];
		for (var i = 0, l = options.length; i < l; i++) {
			if (options[i].selected) {
				if (options[i].value !== "") {
					value.push(options[i].value);
				}
			}
		}
		return value;
	}

	onChange = (event) => {
		const state = this.state
		const target = event.target;
		const name = target.name;
		var value = "";

		if (target.type == "select-multiple"){
			value = this.getSelectedOptions(target.options)
		} else {
			value = target.value;
		}

		state[name]["value"] = value;

		if (value === "" || value.length === 0){
			state[name]["is_valid"] = "error";
			state[name]["error"] = "This Field is Required.";
		} else {
			state[name]["is_valid"] = "success";
			state[name]["error"] = "";
		}

		if (state.server_name.value !== "" && state.server_access.value.length !== 0){
			state["is_disabled"]["value"] = false;
		} else {
			state["is_disabled"]["value"] = true;
		}
		this.setState(state);
	}

	handleOnSubmit = (event) => {
		event.preventDefault();
		const that = this;
		const state = that.state;
		state["is_loading"] = true;
		state["is_disabled"]["value"] = true;
		that.setState(state);

		const data = {
			server_name: state.server_name.value,
			server_ip: state.server_ip.value,
			server_username: state.server_username.value,
			server_password: state.server_password.value,
			server_access: state.server_access.value
		}
		fetch('http://localhost:5000/api/create-server/', {
			headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
			method: 'POST',
			body: JSON.stringify(data),
		}).then(res => {
			if(res.status === 200) {
				const new_state = getInitialState();
				new_state["msg"]["value"] = true;
				new_state["users"] = state.users;
				that.setState(new_state);
				ReactDOM.findDOMNode(this.refs.server_access).value = '';
			} else {
				that.setState(getInitialState());
			};
		});
	}

	componentWillMount() {
		loginRequired();
		adminRequired();
		this.fetchUsers("reactjs");
	}

  	render() {
  		const is_msg = this.state.msg.value;
  		const is_loading = this.state.is_loading;
    	return (
		<div>
	        <Grid>
	      		<Row>
	      			<Col xs={6} md={5} lg={6} mdOffset={3}>
	      				<h2>Group Add</h2>
	      				{ is_msg &&
		      				<Alert bsStyle="success" onDismiss={this.handleDismiss}>
		      					<p>
		      						Server Added Successfully.
		      					</p>
					        </Alert>
				    	}    	
			    		<Form horizontal onSubmit={this.handleOnSubmit}>
						    <FormGroup controlId="formHorizontalGNAME" validationState={ this.state.server_name.is_valid }>
						    	<Col componentClass={ControlLabel} sm={2}>Server Name
						    	</Col>
						    	<Col sm={10}>
						    		<FormControl type="Text" placeholder="Server Name" onChange={this.onChange} name="server_name" value={ this.state.server_name.value } />
						    		<FormControl.Feedback />
						      		<HelpBlock>{ this.state.server_name.error }</HelpBlock>
						      		{ is_loading &&
						      			<Spinner name="three-bounce" />
						      		}
						    	</Col>
						    </FormGroup>
						    <FormGroup controlId="formHorizontalSIP" validationState={ this.state.server_ip.is_valid }>
						    	<Col componentClass={ControlLabel} sm={2}>Server IP Address
						    	</Col>
						    	<Col sm={10}>
						    		<FormControl type="Text" placeholder="Server IP Address" onChange={this.onChange} name="server_ip" value={ this.state.server_ip.value } />
						    		<FormControl.Feedback />
						      		<HelpBlock>{ this.state.server_ip.error }</HelpBlock>
						    	</Col>
						    </FormGroup>
						    <FormGroup controlId="formHorizontalSUN" validationState={ this.state.server_username.is_valid }>
						    	<Col componentClass={ControlLabel} sm={2}>Server IP Address
						    	</Col>
						    	<Col sm={10}>
						    		<FormControl type="Text" placeholder="Server Username" onChange={this.onChange} name="server_username" value={ this.state.server_username.value } />
						    		<FormControl.Feedback />
						      		<HelpBlock>{ this.state.server_username.error }</HelpBlock>
						    	</Col>
						    </FormGroup>
						    <FormGroup controlId="formHorizontalSPASS" validationState={ this.state.server_password.is_valid }>
						    	<Col componentClass={ControlLabel} sm={2}>Server Password
						    	</Col>
						    	<Col sm={10}>
						    		<FormControl type="password" placeholder="Server Password" onChange={this.onChange} name="server_password" value={ this.state.server_password.value } />
						    		<FormControl.Feedback />
						      		<HelpBlock>{ this.state.server_password.error }</HelpBlock>
						    	</Col>
						    </FormGroup>
						    <FormGroup controlId="formControlsSelectMultipleGM"  validationState={ this.state.server_access.is_valid }>
						    	<Col componentClass={ControlLabel} sm={2}>Group Members
						    	</Col>
						    	<Col sm={10}>
						    		<FormControl componentClass="select" multiple onChange={this.onChange} name="server_access" ref="server_access">
						    			<option value="">select (multiple)</option>{
						    				this.state.users.map((user, index) => 
						    					<option value={user.uid}>{user.first_name} {user.last_name}</option>
						    					)}
						    		</FormControl>
						    		<FormControl.Feedback />
						      		<HelpBlock>{ this.state.server_access.error }</HelpBlock>
					    		</Col>
						    </FormGroup>
						    <FormGroup>
						    	<Col sm={12}>
						    		<Button bsStyle="primary" disabled={this.state.is_disabled.value} type="submit">Submit</Button>
						    	</Col>
						    </FormGroup>
						</Form>
					</Col>
				</Row>
			</Grid>
      </div>
    );
  }
}

export default ServerAddComponent;