
export const loginRequired = () => {
    var is_loggedin = localStorage.token;
	if (!is_loggedin){
		window.location.href = "/";
	}
}


export const adminRequired = () => {
    var is_admin = localStorage.is_admin;
	if (is_admin !== "true" ){
		window.location.href = "/send-message";
	}
}