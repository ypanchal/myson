import React, { Component }  from 'react';


class LogoutComponent extends Component {

	componentWillMount() {
		localStorage.clear();
		window.location.href = "/";
	}

  render() {
    return null
  }
}

export default LogoutComponent;