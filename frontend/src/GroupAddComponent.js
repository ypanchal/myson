import React, { Component }  from 'react';
import {Grid, Row, Form, FormGroup, FormControl, Col, ControlLabel, controlId, Checkbox, Button, HelpBlock, Alert} from 'react-bootstrap';
import Spinner from 'react-spinkit';
import ReactDOM from 'react-dom';
import {loginRequired, adminRequired} from './LoginRequiredMixin.js';


function getInitialState(){
	return {
    	users: [],
    	group_name: {"value": "", "is_valid": null, "error": ""},
		group_members: {"value": [], "is_valid": null, "error": ""},
		is_disabled: {"value": true},
    	msg: {"value": false},
    	is_loading: false
    };
}


class GroupAddComponent extends Component {

	constructor(props) {
		super(props);

	    this.state = {
	    	users: [],
	    	group_name: {"value": undefined, "is_valid": null, "error": ""},
			group_members: {"value": [], "is_valid": null, "error": ""},
			is_disabled: {"value": true},
	    	msg: {"value": false},
	    	is_loading: false
	    };
	}

	handleDismiss = (event) => {
		const state = this.state
		state["msg"] = false;
		this.setState(state);
	}

	fetchUsers() {
		var that = this;
		fetch("http://localhost:5000/api/users/").then(function (response) {
			return response.json();
		}).then(function (result) {
			that.setState({users: result.users});
		});
	}

	getSelectedOptions(options) {
		var value = [];
		for (var i = 0, l = options.length; i < l; i++) {
			if (options[i].selected) {
				if (options[i].value !== "") {
					value.push(options[i].value);
				}
			}
		}
		return value;
	}

	onChange = (event) => {
		const state = this.state
		const target = event.target;
		const name = target.name;
		var value = "";

		if (target.type == "select-multiple"){
			value = this.getSelectedOptions(target.options)
		} else {
			value = target.value;
		}

		state[name]["value"] = value;

		if (value === "" || value.length === 0){
			state[name]["is_valid"] = "error";
			state[name]["error"] = "This Field is Required.";
		} else {
			state[name]["is_valid"] = "success";
			state[name]["error"] = "";
		}

		if (state.group_name.value !== "" && state.group_members.value.length !== 0){
			state["is_disabled"]["value"] = false;
		} else {
			state["is_disabled"]["value"] = true;
		}
		this.setState(state);
	}

	handleOnSubmit = (event) => {
		event.preventDefault();
		const that = this;
		const state = that.state;
		state["is_loading"] = true;
		state["is_disabled"]["value"] = true;
		that.setState(state);

		const data = {
			group_name: state.group_name.value,
			group_members: state.group_members.value
		}
		fetch('http://localhost:5000/api/create-group/', {
			headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
			method: 'POST',
			body: JSON.stringify(data),
		}).then(res => {
			if(res.status === 200) {
				const new_state = getInitialState();
				new_state["msg"]["value"] = true;
				new_state["users"] = state.users;
				that.setState(new_state);
				ReactDOM.findDOMNode(this.refs.group_members).value = '';
			} else {
				that.setState(getInitialState());
			};
		});
	}

	componentWillMount() {
		loginRequired();
		adminRequired();
		this.fetchUsers("reactjs");
	}

  	render() {
  		const is_msg = this.state.msg.value;
  		const is_loading = this.state.is_loading;
    	return (
		<div>
	        <Grid>
	      		<Row>
	      			<Col xs={6} md={5} lg={6} mdOffset={3}>
	      				<h2>Group Add</h2>
	      				{ is_msg &&
		      				<Alert bsStyle="success" onDismiss={this.handleDismiss}>
		      					<p>
		      						Group Created Successfully.
		      					</p>
					        </Alert>
				    	}    	
			    		<Form horizontal onSubmit={this.handleOnSubmit}>
						    <FormGroup controlId="formHorizontalGNAME" validationState={ this.state.group_name.is_valid }>
						    	<Col componentClass={ControlLabel} sm={2}>Group Name
						    	</Col>
						    	<Col sm={10}>
						    		<FormControl type="Text" placeholder="Group Name" onChange={this.onChange} name="group_name" value={ this.state.group_name.value } />
						    		<FormControl.Feedback />
						      		<HelpBlock>{ this.state.group_name.error }</HelpBlock>
						      		{ is_loading &&
						      			<Spinner name="three-bounce" />
						      		}
						    	</Col>
						    </FormGroup>
						    <FormGroup controlId="formControlsSelectMultipleGM"  validationState={ this.state.group_members.is_valid }>
						    	<Col componentClass={ControlLabel} sm={2}>Group Members
						    	</Col>
						    	<Col sm={10}>
						    		<FormControl componentClass="select" multiple onChange={this.onChange} name="group_members" ref="group_members">
						    			<option value="">select (multiple)</option>{
						    				this.state.users.map((user, index) => 
						    					<option value={user.uid}>{user.first_name} {user.last_name}</option>
						    					)}
						    		</FormControl>
						    		<FormControl.Feedback />
						      		<HelpBlock>{ this.state.group_members.error }</HelpBlock>
					    		</Col>
						    </FormGroup>
						    <FormGroup>
						    	<Col sm={12}>
						    		<Button bsStyle="primary" disabled={this.state.is_disabled.value} type="submit">Submit</Button>
						    	</Col>
						    </FormGroup>
						</Form>
					</Col>
				</Row>
			</Grid>
      </div>
    );
  }
}

export default GroupAddComponent;