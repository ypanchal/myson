$(document).ready(function() {
	// ##FOR MERGING CELLS WITH COMMON TEXT
	if (window.location.href.endsWith('report/onepager/')) {
		$('.report-sidebar li').removeClass('active');
		$('.report-sidebar li:first-child').addClass('active');
	}
	$('#one-pager tbody th:first-child').each(function() {
	    var currentrow = $(this).closest('tr');
	    var prev_text = currentrow.prev().find('th:first-child').text();
	    var next_text = currentrow.next().find('th:first-child').text();
	    selector_ele = $('#one-pager tbody th:first-child:contains("'+$(this).text()+'")')
	    if ($(this).text() == prev_text && $(this).text() != next_text) {
	    	selector_ele.first().attr('rowspan', selector_ele['length']);
	    	$('#one-pager tbody th:first-child:contains("'+$(this).text()+'"):gt(0)').hide();
	    }
	});

	function calculate_max_width(target) {
		var max = 0;    
		$(target).each(function() {
		    max = Math.max($(this).outerWidth(), max);
		}).css({'min-width': max});
	} 

	calculate_max_width('.one-pager-report.first-child-bg td');

	$("td:even").addClass('mis');
	$("td:odd").addClass('report-audited');	
	$('#filter-selector').change(function() {
		if ($(this).val() == 'MIS') {
			$('.report-audited').addClass('hidden');
			$('.mis').removeClass('hidden');
			$('table thead tr:first-child th:not(:first-child)').attr('colspan', '1');
		} else if ($(this).val() == 'Audited') {
			$('.mis').addClass('hidden');
			$('.report-audited').removeClass('hidden');
			$('table thead tr:first-child th:not(:first-child)').attr('colspan', '1');
		} else {
			$('.report-audited').removeClass('hidden');
			$('.mis').removeClass('hidden');
			$('table thead tr:first-child th:not(:first-child)').attr('colspan', '2');
		}
		calculate_max_width('.one-pager-report.first-child-bg thead tr:first-child th:not(:first-child)');
	});

});

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

var cookie_check;
function checkCookie() {
	file_download = getCookie("file_download");
	if (file_download === "true"){
		$("#export_report_loading").addClass("hidden");
		document.cookie = "file_download=false";
		clearInterval(cookie_check);
	}
}

$("#report_export_excel").on("click", function(){
	$("#export_report_loading").removeClass("hidden");
	cookie_check = setInterval(checkCookie, 1000);
})

